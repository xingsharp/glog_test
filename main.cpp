#include <QCoreApplication>
#include <QDebug>
#include <QThread>
#include <glog/logging.h>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug()<<"begin glog ini............";

    // glog run time config. -----------------------
    google::InitGoogleLogging(argv[0]);//Initialize Google's logging library, argv[0] is program name.
        // set Param--------------------------------
            //<way1> 通过GFLAGS来设置参数; FLAGS_xxx = xx
    FLAGS_stderrthreshold = google::ERROR; //INFO WARNING ERROR FATAL, 默认输出到stderr(app Output/cli)的阀值是ERROR
    FLAGS_alsologtostderr = false; //当这个全局变量为真时，忽略FLAGS_stderrthreshold的限制，所有信息打印到终端
    FLAGS_colorlogtostderr = true; //设置输出到屏幕的日志显示相应颜色,错误等级有颜色区分
    FLAGS_max_log_size = 100; //Maximum log size: 100MB
    FLAGS_logbufsecs =0;        //缓冲日志输出，默认为30秒，此处改为立即输出
    FLAGS_stop_logging_if_full_disk = true;     //当磁盘被写满时，停止日志输出
            //<wah2>
    //set log path;第一个参数为日志级别设置,级别高于 google::INFO 的日志同时输出到屏幕，第二个参数表示输出目录及日志文件名前缀,log目录我是事先在build-prj目录下创建好.
    google::SetLogDestination(google::INFO,   "log/INFO_");
    google::SetLogDestination(google::WARNING,"log/WARNING_");   //设置 google::WARNING 级别的日志存储路径和文件名前缀
    google::SetLogDestination(google::ERROR,  "log/ERROR_");    //设置 google::ERROR 级别的日志存储路径和文件名前缀
    google::SetLogDestination(google::FATAL,  "log/FATAL_");    //设置 google::FATAL 级别的日志存储路径和文件名前缀
    google::SetLogFilenameExtension("glog_test_");     //设置文件名扩展，如平台？或其它需要区分的信息


    // glog test -----------------------------------
    qDebug()<<"begin glog test........... ";

    LOG(INFO)<<"LOG(INFO):hello world";
    LOG(INFO)<<"Found "<<google::COUNTER<<endl;
    LOG(WARNING)<<"warning test";
    LOG(ERROR)<<"error test";

    LOG(FATAL)<<"fatal test"; //place here,because the Macro let the program exit.
//    google::ShutdownGoogleLogging();
    return a.exec();
}
